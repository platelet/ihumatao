<!-- Main -->
<div id="main">
  <article class="post featured policy">
    <header class="major">
      <h2>The Week of Climate Justice - 25th to 27th March</h2>
    </header>

    <h2>Sunday 25th March</h2>
    <h3 class="left no-mar-bottom"><b>Information Day: Moving on from Oil and Gas, Nau Mai Haere Mai</b></h3>
    <p><i>25 March at 10:00–17:00</i> | <i>The Dowse Art Museum - 45 Laings Road, Lower Hutt</i> | <a href="https://www.facebook.com/events/278743309324616/" target="_blank">facebook event</a></p>

    <p>Frack Free Network has organised a rare opportunity to learn more about the connections between NZ’s oil and gas industry and: sacrificed communities, endangered marine life, water issues, man-made climate change and our backyard BBQ’s. Sign up to reserve a seat or just call in.</p>
    <p>Leading scholars, activists and scientists will present the latest information available throughout the day and answer the hard questions like: how can I join the clean energy movement when I drove here in a car, is gas a good transition fuel, and is the Government addressing climate change or passing the buck?</p>
    <p>Speakers include: Ihaia Puketapu, Teanau Tuiono, Sarah Roberts, Dr Mike Joy, Jeanette Fitzsimons, Dr Lyndon DeVantier, Fiona Clark and more.</p>

    <h2>Monday 26th March</h2>

    <h3 class="left no-mar-bottom"><b>Opening Ceremony of the Rally for Climate Justice</b></h3>
    <p><i>26 March at 12:15–13:15</i> | <i>Meet at Frank Kitts Parks - Jervious Quay Wellington</i> | <a href="https://www.facebook.com/events/214427575775482/" target="_blank">facebook event</a></p>

    <p>Join our friends at 350 NZ for the Opening Ceremony of the Rally for Climate Justice on Monday March 26th, as they gather outside the Petroleum Conference in Wellington to show their opposition to more oil and gas projects in New Zealand. They will piece together a giant sunflower, representing different communities and groups - Doctors, Church leaders, Grandparents, Wellingtonians and many more - united in the face of climate change, and in their demand for an end to new oil and gas projects in Aotearoa.</p>
    <p>They have chosen the symbolism of the sunflower because it can both carry the message of resistance to oil and gas expansion, and a vision for a brighter future, powered by the sun and wind. Sunflowers have also been used as a symbol of the climate movement in other parts of the world.</p>

    <h3 class="left no-mar-bottom"><b>Blockade Briefing/NVDA Training</b></h3>
    <p><i>26 March at 18:00-21:00</i> | <i>Downstairs Hall, Wellington Central Baptist Church, Boulcott St</i>

    <p>Join us the night before the Rally and blockade for some Non-Violent Direct Action training and action briefing to help you prepare for the Rally and meet your fellow activists. Please bring a plate to share.</p>

    <h2>Tuesday 27th March</h2>

    <h3 class="left no-mar-bottom"><b>Rally for Climate Justice - Stop the Oil Summit</b></h3>
    <p><i>27 March at 7:00–16:00</i> | <i>Meet at Frank Kitts Parks - Jervious Quay Wellington</i> | <a href="https://www.facebook.com/events/359022637895815/" target="_blank">facebook event</a></p>

    <p>On Tuesday 27th March people from all around Aotearoa will gather for a rally and blockade of the oil conference. We will demand climate justice: working for solutions with those who are most affected.</p>
    <p>We are bringing together a range of groups to stand up for climate justice, including iwi, faith groups, grassroots campaigners, climate NGOs, and the Supergrans.</p>
    <p>We ask that you support the <a href="/principles">Principles of the Rally</a>, and be respectful of how others choose to protest. To find out more, check out our <a href="/safer_spaces">Safer Spaces policy</a>.</p>
    <p><b>Things to bring:</b></p>
    <ul>
      <li>Food and drink. We will have food being prepared at the blockade but having snacks never goes amiss. There is a water source close to the TSB Arena</li>
      <li>Layers of clothes for the changing Wellington weather; a hat; sunscreen</li>
      <li>A cushion, or something to sit on</li>
      <li>Placards, signs, or banners that get your message across - why YOU think it's important to stand up for climate justice!</li>
      <li>An idea of how you would like to participate in the rally/blockade</li>
      <li>A buddy. It's important to have support at protests, especially if you are planning on blockading. Stick together, keep checking in emotionally, and keep communicating throughout the day</li>
    </ul>

    <h3 class="left no-mar-bottom"><b>Affinity Group Events during the Blockade</b></h3>
    <h4 class="left no-mar-bottom"><b>Critical Mass</b></h4>
    <p><i>7:30am - 9:30am</i> | <i>Meet at Frank Kitts Parks - Jervious Quay Wellington</i> | <a href="https://www.facebook.com/events/939309509569803/" target="_blank">facebook event</a></p>
    <p>Want to disrupt the oil summit with zero carbon activity? Love to cycle? Join this early-morning critical mass cycle ride disrupting the Petroleum industry conference.</p>

    <h4 class="left no-mar-bottom"><b>Supergrans say "No more oil drilling!"</b></h4>
    <p><i>7:30 am</i> | <i>Meet at Frank Kitts Parks - Jervious Quay Wellington</i> | <a href="https://www.facebook.com/events/157897234913640/" target="_blank">facebook event</a></p>
    <p>Grandpas, Grandmas, Grand-aunts & uncles who are worried about the impact of climate change on our grandkids can find strength (and fun) in numbers. Many of us plan to get together at the Oil Summit Blockade in Wellington on Monday 26th March. We plan to form a distinctive group, possibly walking single file to pipe and drum around the blockade. Some of us may choose to join the actual physical blockade. That is optional. (NB the times stated are very rough as yet!)</p>
    <p>Want to keep in touch & find out more? Email Joanna Santa Barbara at joanna@atamaivillage.nz, and/or FB post to this event.</p>

    <h3 class="left no-mar-bottom"><b>Blockade De-brief</b></h3>
    <p><i>27 March at 17:00-19:00</i> | <i>Sustainability Trust, Forresters Lane (off Tory St)</i></p>

    <p>Please join us for a debrief after the rally and blockade. It's really important to share your experiences with people who've just been through the same thing, and helps us plan for next steps and future events in dismantling the New Zealand petroleum industry. It's also important to think about ways that you'll take care of yourself and those around you in the aftermath of events like these. There will be hot drinks and snacks.</p>

  </article>
</div>
