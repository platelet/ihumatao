<!-- Main -->

<script>
  function download() {
    var link = document.createElement("a");
    link.setAttribute("href", window.location.hostname+"/download/registration_csv");
    link.setAttribute("download", "ihumatao_registrations.csv");
    link.click();
  }
</script>
<div id="main">
  <section style="overflow: auto;">
    <h2>ADMIN</h2>
    <a class="fileDownloadPromise" href="/download/registration_csv"><button>Download Data as CSV</button></a>
    <br>
    <br>
    <table>
      <thead>
        <tr>
          <th colspan="5"><u>Contact Details</u></th>
          <th colspan="7"><u>Coming to the Whenua</u></th>
          <th colspan="3"><u>Other Support</u></th>
        </tr>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Iwi</th>
          <th>Organisation</th>
          <th>Registration Type</th>
          <th>Arrival Date</th>
          <th>Registering On Behalf</th>
          <th>Number Registering</th>
          <th>Emergency Contact Name</th>
          <th>Emergency Contact Number</th>
          <th>Need to know</th>
          <th>Volenteer Roles</th>
          <th>Volunteer Comment</th>
          <th>Created At</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $row) { ?>
        <tr>
          <td><?php echo $row['name']; ?></td>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo $row['phone']; ?></td>
          <td><?php echo $row['region']; ?></td>
          <td><?php echo $row['taamaki']; ?></td>
          <td><?php echo $row['registrationType']; ?></td>
          <td><?php echo $row['arriveOn']; ?></td>
          <td><?php echo $row['onBehalf']; ?></td>
          <td><?php echo $row['numOfRegisters']; ?></td>
          <td><?php echo $row['emergencyName']; ?></td>
          <td><?php echo $row['emergencyNumber']; ?></td>
          <td><?php echo $row['needToKnow']; ?></td>
          <td><?php echo $row['volunteerRoles']; ?></td>
          <td><?php echo $row['volunteerComments']; ?></td>
          <td><?php echo $row['createdAt']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </section>
</div>
