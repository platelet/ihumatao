<!-- Main -->
<div id="main">
    <article class="post featured">
      <header class="major">
        <h2>Our Principles</h2>
      </header>
      <p>We recognise the urgency of acting on climate change and momentum built around this over the years. We will escalate action to disrupt, and promote resistance to, the oil and gas industries and to keep fossil fuels in the ground. We will use a variety of techniques to disrupt the petroleum summit and strengthen the movement that challenges the corporate and colonial systems that perpetuate business as usual, with an overall goal of climate justice.</p>
      <p>We acknowledge mana whenua as the rightful kaitiaki of Aotearoa, and recognise that tino rangatiratanga was never ceded. We support tangata whenua leadership to tackle the systems that oppress and inflict injustice on Indigenous populations. We acknowledge that our activism is taking place on stolen land. We aim to decolonise our own practice and worldviews, and to build partnerships with those resisting the colonisation of land and people.</p>
      <p>Climate justice means an acknowledgement that climate change is a product of socio-economic and political systems, like capitalism, patriarchy and colonialism. Therefore, it can’t be adequately addressed through individual action (e.g. green consumption) or technology.</p>
      <p>Climate justice begins from a recognition that those most affected by climate change are the least responsible for it. For instance, Indigenous communities and people of colour, women, the working class and economically marginalised, and children are most likely to bear the brunt of a changing climate. We are in solidarity with our Pacific neighbours who are already experiencing the effects of, and resisting, climate change.</p>
      <p>Climate justice action seeks to end systems of oppression and prioritise solutions envisioned by communities that are most affected by change, and the costs of these solutions are borne equitably.</p>
    </article>
</div>
