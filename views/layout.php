<!DOCTYPE HTML>
<html>
	<head>
		<meta property="og:url" content="https://register.protectihumatao.com<?php echo $oglink ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Register to Protect Ihumaatao" />
		<meta property="og:description" content="Register your support in the occupation of Ihūmatao" />
		<meta property="og:image" content="https://protectihumaatao.s3-ap-southeast-2.amazonaws.com/assets/bewareo.png" />
		<meta property="og:image:secure_url" content="https://protectihumaatao.s3-ap-southeast-2.amazonaws.com/assets/bewareo.png" />
		<meta name="title" content="Register to Protect Ihumaatao" />
		<meta name="description" content="Register your support in the occupation of Ihūmatao" />
		<link rel="image_src" href="https://protectihumaatao.s3-ap-southeast-2.amazonaws.com/assets/bewareo.png" />
		<meta name="keywords" content="protect ihuumatao land occupation">
		<meta name="robots" content="noarchive">
		<title>Save Our Unique Landscape | <?php echo $title ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-loading" onload="bind()">

		<!-- Wrapper -->
			<div id="wrapper" class="fade-in">

				<!-- HEADER CONTENT ## $header_content -->
        <?php echo $header_content; ?>

        <!-- BODY CONTENT ## $body_content -->
				<?php echo $body_content; ?>

				<div id="page_buffer"></div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.download.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

			<script>
				function bind() {
					$(document).on("click", "a.fileDownloadPromise", function () {
				    $.fileDownload($(this).prop('href'))
				      .done(function () { alert('File download a success!'); })
				      .fail(function () { alert('File download failed!'); });

				    return false;
				  });
				}
			</script>

	</body>
</html>
