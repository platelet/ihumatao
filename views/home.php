<!-- Main -->
<div id="main">

  <!-- Featured Post -->
    <article class="post featured">
      <header class="major">
        <h2>Register to Protect Ihumaatao</h2>
      </header>
      <h3>Nau mai, Haere mai, tautoko mai</h3>
      <br>

      <p><strong>As events are unfolding quickly, our <a href="https://www.facebook.com/protectihumatao">Facebook page</a> is the most immediate way to follow what is happening</strong></p>
      <p>The mana whenua-lead campaign to protect Ihumaatao has always upheld the kaupapa of Rongo (peace), taking every possible step to avoid a confrontation on the whenua.</p>
      <p>Police have now removed kaitiaki and taken control of the site to allow Fletcher Residential to begin development. We need support now to protect the whenua, while continuing to use peaceful, non-violent but direct means.</p>
      <p>Now is the time to stand together and protect Ihumaatao. Follow the link below to register your support to stand with us in person.</p>

      <button type="button" name="button">
        <a href="/register">Register Now!</a>
      </button>
      <button type="button" name="button">
        <a href="https://www.protectihumatao.com/">Learn more about Ihumaatao!</a>
      </button>

      <!-- <ul class="actions">
        <li><a href="#" class="button big">Join the group</a></li>
        <li><a href="#" class="button big">Pledge to Act</a></li>
        <li><a href="#" class="button big">Save the Date</a></li>
      </ul> -->
    </article>

</div>

<!-- Footer -->
<footer id="footer">
  <section style="text-align: center;">
    <div id="copyright">
      <ul><li>anti-&copy;</li></ul>
    </div>
  </section>
</footer>
