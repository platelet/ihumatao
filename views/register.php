<!-- Main -->
<div id="main">
  <section>
    <h2>Register here to protect Ihumaatao</h2>
    <form class="alt" method="post" action="/register">
      <h3>Your Contact Information</h3>
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="name">Name</label>
          <input type="text" name="name" id="name" />
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="email">Email</label>
          <input type="text" name="email" id="email" />
        </div>
      </div>
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="name">Phone Number</label>
          <input type="text" name="phone" id="phone" />
        </div>

      </div>
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="region">Hapu/Iwi</label>
          <input type="text" name="region" id="region" />
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="taamaki">Organisation (if applicable)</label>
          <input type="text" name="taamaki" id="taamaki" />
        </div>
      </div>

      <br>
      <br>
      <h3>Coming to the Whenua</h3>
      <p>If you are supporting the occupation by coming to the whenua, please fill out the following details</p>
      <div class="row uniform">
        <div class="12u$ 12u$(xsmall)">
          <input type="radio" name="registrationType" id="inPerson" value="In Person" />
          <label for="inPerson">I am registering in person on the whenua.</label>
          <input type="radio" name="registrationType" id="inAdvance" value="In Advance" />
          <label for="inAdvance">I am registering in advance.</label>
        </div>
      </div>
      <div class="row uniform">
        <div class="12u 12u$(xsmall)">
          <label for="arriveOn">If registering in advance I will arrive on:</label>
          <input type="date" name="arriveOn" id="arriveOn" />
        </div>
      </div>

      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label name="__" for="__">&nbsp;</label>
          <input type="checkbox" name="onBehalf" id="onBehalf" />
          <label for="onBehalf">Are you registering on behalf of others?</label>
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="numOfRegisters">How many people are you registering?</label>
          <input type="number" name="numOfRegisters" id="numOfRegisters" />
        </div>
      </div>

      <br>
      <div class="row uniform">
        <div class="6u 12u$(xsmall)">
          <label for="emergencyName">Emergency Contact Name / Next of Kin</label>
          <input type="text" name="emergencyName" id="emergencyName" />
        </div>
        <div class="6u$ 12u$(xsmall)">
          <label for="emergencyNumber">Emergency Contact's Number</label>
          <input type="text" name="emergencyNumber" id="emergencyNumber" />
        </div>
      </div>
      <div class="row uniform">
        <div class="12u 12u$(xsmall)">
          <label for="needToKnow">Is there anything else we need to know? e.g. accessibility or medical conditions</label>
          <input type="text" name="needToKnow" id="needToKnow" />
        </div>
      </div>

      <br>
      <br>
      <h3>Other support that may be needed</h3>
      <p style="margin: 0;">If you wish to make a donation to the occupation please use the following bank account:</p>
      <blockquote>Bank: Kiwibank<br>Account Name: SOUL<br>Account Number: 38-9017-0062452-00</blockquote>
      <div class="row uniform">
        <div class="12u 12u$(xsmall)">
          <input type="checkbox" id="Administration" name="volunteerRoles[]" value="Administration" />
          <label for="Administration">Administration</label>
          <input type="checkbox" id="Building" name="volunteerRoles[]" value="Building" />
          <label for="Building">Building</label>
          <input type="checkbox" id="Cooking" name="volunteerRoles[]" value="Cooking" />
          <label for="Cooking">Cooking</label>
          <input type="checkbox" id="firstAid" name="volunteerRoles[]" value="First Aid" />
          <label for="firstAid">First Aid</label>
          <input type="checkbox" id="Gardening" name="volunteerRoles[]" value="Gardening" />
          <label for="Gardening">Gardening</label>
          <input type="checkbox" id="Electrical" name="volunteerRoles[]" value="Electrical work" />
          <label for="Electrical">Electrical work</label>
          <input type="checkbox" id="Food" name="volunteerRoles[]" value="Food Donations" />
          <label for="Food">Food donations (cooked or ready to eat)</label>
          <input type="checkbox" id="Time" name="volunteerRoles[]" value="Time" />
          <label for="Time">Time</label>
          <input type="checkbox" id="Letter" name="volunteerRoles[]" value="Letter writing" />
          <label for="Letter">Letter writing</label>
          <input type="checkbox" id="Phoning" name="volunteerRoles[]" value="Phoning politicians" />
          <label for="Phoning">Phoning politicians</label>
          <input type="checkbox" id="Pickets" name="volunteerRoles[]" value="Pickets" />
          <label for="Pickets">Pickets at other venues</label>
          <input type="checkbox" id="Facilitation" name="volunteerRoles[]" value="Facilitation" />
          <label for="Facilitation">Group facilitation</label>
          <input type="checkbox" id="Activities" name="volunteerRoles[]" value="Activities" />
          <label for="Activities">Running activities</label>
        </div>
      </div>
      <div class="row uniform">
        <div class="12u 12u$(xsmall)">
          <label for="volunteerComments">Please describe in more detail if needed:</label>
          <input type="text" name="volunteerComments" id="volunteerComments" />
        </div>
      </div>

      <div class="row uniform">
        <ul class="actions">
          <li><input type="submit" value="Submit" /></li>
        </ul>
      </div>
      <small><strong><i>Your data will be held on a secure server, and deleted after the occupation</i></strong></small>
    </form>
  </section>
</div>
