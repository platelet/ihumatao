<!-- Intro -->
<?php if ($page === 'home') { ?>
<div id="intro">
  <h1>Save Our Unique Landscape</h1>
  <h2 style="font-size: 3.5em;">Protect Ihūmatao</h2>
  <p>Toi tuu te whenua, whatungarongaro te tangata</p>
  <ul class="actions">
    <li><a href="#header" class="button icon solo fa-arrow-down scrolly">Continue</a></li>
  </ul>
</div>
<?php } ?>

<!-- Header -->
<header id="header">
  <?php if ($page === 'home') { ?>
    <a href="/" class="logo">Save Our Unique Landscape</a>
  <?php } else { ?>
    <a href="/" class="logo">Save Our Unique Landscape<br /> | <?php echo $page ?></a>
  <?php } ?>
</header>

<!-- Nav -->
<nav id="nav">
  <ul class="links">
    <li <?php if ($page === 'home') echo 'class="active"'; ?>><a href="/">About</a></li>
    <li <?php if ($page === 'register') echo 'class="active"'; ?>><a href="/register">Register</a></li>
    <li><a href="https://www.protectihumatao.com/">More Information</a></li>
  </ul>
  <ul class="icons">
    <li><a href="https://www.facebook.com/protectihumatao/" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
  </ul>
</nav>
