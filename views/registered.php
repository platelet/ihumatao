<!-- Main -->
<div id="main">
  <section>
    <h2>Registered!</h2>
    <p>Thank you for registering to take action. You will recieve an email shortly with information, if you don't recieve it please check your spam folder</p>
    <p>You can contact SOUL via <a href="mailto:saveihumatao@gmail.com">Email</a> or <a href="https://www.facebook.com/protectihumatao/">Facebook (updates posted here)</a></p>
  </section>
</div>
