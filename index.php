<?php

  require 'flight/Flight.php';
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  require 'PHPMailer/src/Exception.php';
  require 'PHPMailer/src/PHPMailer.php';
  require 'PHPMailer/src/SMTP.php';

  error_reporting(0);

  //restart any current browser sessions
	session_start();
  if (!$_SESSION['user_defined_admin_in']) {
    $_SESSION['user_defined_admin_in'] = 'NO';
  }

	/* MYSQL DATABASE CONNECTION */
	//database creditails

  define('DB_USER'      , getenv('DATABASE_USER'));
	define('DB_PASS'      , getenv('DATABASE_PASSWORD'));
	define('DB_NAME'      , getenv('DATABASE_NAME'));
	define('DB_HOST'      , getenv('DATABASE_HOST'));
  define('ADMIN_PASS'   , getenv('ADMIN_PASSWORD'));
  define('EMAIL_PASS'   , getenv('EMAIL_PASSWORD'));
  define('EMAIL_ADDRESS', getenv('EMAIL_ADDRESS'));

	//connect to MySQL
  $mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
  if (mysqli_connect_errno($mysqli)) {
    die("Failed to connect to MySQL: " . mysqli_connect_error());
  }

  Flight::set('flight.views.path', 'views/');

  Flight::route('/.well-known/acme-challenge/@challengeHash', function($challengeHash) {
    echo $challengeHash.'.tvZoTWuQgfiNyjL60B8YO3MBL4hJ0Kt6-rCTO_Pi5AQ';
  });

  Flight::route('/', function(){
    Flight::render('header', array('page' => 'home'), 'header_content');
    Flight::render('home', array(), 'body_content');
    Flight::render('layout', array('oglink' => '/', 'title' => 'Stop the Oil Summit'));
  });

  Flight::route('/about', function(){
    Flight::render('header', array('page' => 'about'), 'header_content');
    Flight::render('about', array(), 'body_content');
    Flight::render('layout', array('oglink' => '/about', 'title' => 'About'));
  });

  Flight::route('/principles', function () {
    Flight::render('header', array('page' => 'principles'), 'header_content');
    Flight::render('principles', array(), 'body_content');
    Flight::render('layout', array('title' => 'Principles'));
  });

  Flight::route('/safer_spaces', function () {
    Flight::render('header', array('page' => 'safer'), 'header_content');
    Flight::render('safer_spaces', array(), 'body_content');
    Flight::render('layout', array('title' => 'Safer Spaces Policy'));
  });

  Flight::route('/events', function () {
    Flight::render('header', array('page' => 'events'), 'header_content');
    Flight::render('events', array(), 'body_content');
    Flight::render('layout', array('title' => 'Week of Climate Justice'));
  });

  Flight::route('GET /register', function () {
    Flight::render('header', array('page' => 'register'), 'header_content');
    Flight::render('register', array(), 'body_content');
    Flight::render('layout', array('oglink' => '/register', 'title' => 'Register'));
  });
  Flight::route('GET /registered', function () {
    Flight::render('header', array('page' => 'registered'), 'header_content');
    Flight::render('registered', array(), 'body_content');
    Flight::render('layout', array('title' => 'Registered'));
  });

  function array_to_csv_download($array, $filename = "ihumatao_registrations.csv", $delimiter=";") {
    header("Cache-Control: must-revalidate, pre-check=0");
    header("Cache-Control: private", false);
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    $f = fopen('php://output', 'w');

    foreach ($array as $line) {
      fputcsv($f, $line, $delimiter);
    }
  }

  Flight::route('GET /download/registration_csv', function () {
    global $mysqli;
    if ($_SESSION['user_defined_admin_in'] == 'confirmed') {
      $query = "SELECT * FROM registers WHERE 1";
      $res = mysqli_query($mysqli, $query);
      $data = [['Name','Email','Phone','Iwi','Organisation','Registration Type','Arrival Date','Registering On Behalf','Number Registering','Emergency Contact Name','Emergency Contact Number','Need to know','Volenteer Roles','Volunteer Comment','Created At']];
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($data, [$row['name'],$row['email'],$row['phone'],$row['region'],$row['taamaki'],$row['registrationType'],$row['arriveOn'],$row['onBehalf'],$row['numOfRegisters'],$row['emergencyName'],$row['emergencyNumber'],$row['needToKnow'],$row['volunteerRoles'],$row['volunteerComments'],$row['createdAt']]);
      }
      array_to_csv_download($data);
    }
  });

  Flight::route('GET /manage', function () {
    global $mysqli;
    if ($_SESSION['user_defined_admin_in'] == 'confirmed') {
      $query = "SELECT * FROM registers WHERE 1";
      $res = mysqli_query($mysqli, $query);
      $data = [];
      while ($row = mysqli_fetch_assoc($res)) {
        array_push($data, $row);
      }
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_view', array('data' => $data), 'body_content');
      Flight::render('layout', array('title' => 'Admin'));
    } else {
      Flight::render('header', array('page' => 'admin'), 'header_content');
      Flight::render('admin_login', array(), 'body_content');
      Flight::render('layout', array('title' => 'Admin Login'));
    }
  });
  Flight::route('POST /manage', function () {
    $request = Flight::request();
    $form_data = $request->data;
    $password = $form_data->password;
    if ($password == ADMIN_PASS) {
      $_SESSION['user_defined_admin_in'] = 'confirmed';
      Flight::redirect('/manage');
    } else {
      die('permission denied');
    }
  });

  function sendEmail($name, $email) {
    $mail = new PHPMailer(true);
    try {
      //Server settings
      $mail->SMTPDebug = 2;
      $mail->isSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true;
      $mail->Username = EMAIL_ADDRESS;
      $mail->Password = EMAIL_PASS;
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;

      //Recipients
      $mail->addReplyTo('saveihumatao@gmail.com', 'Save Our Unique Landscape - SOUL');
      $mail->setFrom(EMAIL_ADDRESS, '[no-reply] Save Our Unique Landscape');
      $mail->addAddress($email, $name);

      $mail->isHTML(true);
      $mail->Subject = 'Protect Ihumaatao - Important Information';

      $body = '<p>Kia ora '.$name.'!</p><br/>';
      $body .= '<p>Thank you for signing up to protect Ihumaatao in a very practical way.</p>';
      $body .= '<p>The kaupapa of Rongo (Peace) has been upheld throughout the duration of the campaign, and must be at the heart of any actions taken on the whenua.</p>';
      $body .= '<p>This is an evolving situation, so please read these guidelines for when you arrive on the whenua.</p>';
      $body .= '<b><h4>What to bring</h4></b><p>We ask that you be as self-sufficient as possible when coming to Ihumaatao so as not to place an extra burden on the kaitiaki to support you.</p>';
      $body .= '<p>When coming for the day, please bring your own lunch and water.</p><p>If staying for a longer period, you will need to check whether there is a possibility of accommodation at or near the whenua. Please be prepared with your own equipment and know where you are going to stay.</p>';
      $body .= '<b><h4>Kawa (protocol)</h4></b><p>We are in the realm of Rongo, we are strictly a non-violent direct action campaign and all visitors will be asked to keep to this kaupapa to uphold the integrity of the mana whenua and for the safety and well-being of all manuhiri who come to the whenua.</p>';
      $body .= '<p>This kawa includes no verbal or physical violence, whether against other supporters, the police or contractors we may come into contact with.</p>';
      $body .= '<p><u>We ask that none of the following be brought onto the whenua:</u><br/>No alcohol<br/>No drugs<br/>No weapons.</p>';
      $body .= '<p>It is also important that each person take responsibility for managing their behaviour and emotions in potentially confrontational situations.</p>';
      $body .= '<p>Please bear in mind that anything we do at the whenua reflects on mana whenua and our ability to continue a presence as kaitiaki.</p>';
      $body .= '<b><h4>Media / communications</h4></b>';
      $body .= '<p>Mana whenua will have designated people responsible for speaking to media or liaising with police or any developers that might show up, and a carefully thought-out strategy for managing these communications.</p>';
      $body .= '<p>Please leave these tasks to the delegated people.</p>';
      $body .= '<b><h4>Further updates</h4></b><p>If we need to get in touch with you, we will do this via the contact details you have provided. <b>Your responses are being held on a secure server.</b></p>';
      $body .= '<p>As events are unfolding quickly, our <a href="https://www.facebook.com/protectihumatao/">Facebook page</a> is the most immediate way to follow what is happening.<p><br/>';
      $body .= '<p>Kia kaha, kia maia, kia manawanui! Tiakina Ihumaatao! Toitu te whenua!</p>';
      $body .= '<p>Kaitiaki of Ihumaatao /<br/>SOUL Campaign</p>';

      $mail->Body    = $body;

      $mail->send();
      return true;
    } catch (Exception $e) {
      return $mail->ErrorInfo;
    }
  }

  Flight::route('POST /register', function() {
    global $mysqli;
    $request = Flight::request();
    $form_data = $request->data;

    $search = [';', '%', '*', "'", '"'];
    $replace = ['\;', '\%', '\*', "\'", '\"'];
    //contact
    $name   = str_replace($search, $replace, $form_data->name);
    $email  = str_replace($search, $replace, $form_data->email);
    $phone  = str_replace($search, $replace, $form_data->phone);
    $region = str_replace($search, $replace, $form_data->region);
    $taamaki  = str_replace($search, $replace, $form_data->taamaki);
    $manaWhenua  = str_replace($search, $replace, $form_data->manaWhenua);
    //whenua
    $registrationType = str_replace($search, $replace, $form_data->registrationType);
    $arriveOn = str_replace($search, $replace, $form_data->arriveOn);
    $onBehalf = str_replace($search, $replace, $form_data->onBehalf);
    $numOfRegisters = str_replace($search, $replace, $form_data->numOfRegisters);
    $individualNames = str_replace($search, $replace, $form_data->individualNames);
    $emergencyName = str_replace($search, $replace, $form_data->emergencyName);
    $emergencyNumber = str_replace($search, $replace, $form_data->emergencyNumber);
    $needToKnow = str_replace($search, $replace, $form_data->needToKnow);
    $dayRoster = str_replace($search, $replace, $form_data->dayRoster);
    $nightRoster = str_replace($search, $replace, $form_data->nightRoster);
    $stayComment = str_replace($search, $replace, $form_data->stayComment);
    $stayFor = str_replace($search, $replace, $form_data->stayFor);
    //taking action
    $colorCode = str_replace($search, $replace, $form_data->colorCode);
    //other suport
    $volunteerRoles = str_replace($search, $replace, implode(', ', $form_data->volunteerRoles));
    $volunteerComments = str_replace($search, $replace, $form_data->volunteerComments);

    $query = "INSERT INTO registers (name, email, phone, region, taamaki, manaWhenua, registrationType, arriveOn, onBehalf, numOfRegisters, individualNames, emergencyName, emergencyNumber, needToKnow, dayRoster, nightRoster, stayComment, stayFor, colorCode, volunteerRoles, volunteerComments) VALUES ";
    $query .= "('".$name."', '".$email."', '".$phone."', '".$region."', '".$taamaki."', '".$manaWhenua."', '".$registrationType."', '".$arriveOn."', '".$onBehalf."', '".$numOfRegisters."', '".$individualNames."', '".$emergencyName."', '".$emergencyNumber."', '".$needToKnow."', '".$dayRoster."', '".$nightRoster."', '".$stayComment."', '".$stayFor."', '".$colorCode."', '".$volunteerRoles."', '".$volunteerComments."');";

    if (mysqli_query($mysqli, $query) === TRUE) {
      $e_res = sendEmail($name, $email);
      if ($e_res === true) {
        Flight::redirect('/registered', 402);
      } else {
        die($e_res);
        Flight::redirect('/error', 401);
      }
    } else {
      Flight::redirect('/error', 401);
    }
  });

  Flight::route('/resources', function () {
    Flight::render('header', array('page' => 'resources'), 'header_content');
    Flight::render('resources', array(), 'body_content');
    Flight::render('layout', array('title' => 'Resources'));
  });

  Flight::start();

?>
